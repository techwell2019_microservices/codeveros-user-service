const mongoose = require('mongoose');
const validator = require('validator');

const Schema = mongoose.Schema;

const schema = new Schema({
  username: {
    type: String,
    required: true,
    index: {
      unique: true,
      sparse: true
    }
  },
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    validate: [ validator.isEmail, 'invalid email' ]
  }
}, {
  toJSON: {
    versionKey: false
  }
});

module.exports = mongoose.model('User', schema);
